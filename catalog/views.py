from django.shortcuts import render
from .models import Book, Author, BookInstance, Genre
from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin


# Create your views here.
def index(request):
	"""
    Función vista para la página inicio del sitio.
    """
	# Genera contadores de algunos de los objetos principales

	num_books = Book.objects.all().count()
	num_instances = BookInstance.objects.all().count()
	# Libros disponibles (status = 'a')
	num_instances_available = BookInstance.objects.filter(status__exact='a').count()
	num_authors = Author.objects.count()  # El 'all()' esta implícito por defecto.
	num_visits = request.session.get('num_visits', 0)
	request.session['num_visits'] = num_visits + 1

	context = {
		'num_books': num_books,
		'num_instances': num_instances,
		'num_instances_available': num_instances_available,
		'num_authors': num_authors,
		'num_visits': num_visits
	}

	# Renderiza la plantilla HTML index.html con los datos en la variable contexto
	return render(request, 'catalog/index.html', context=context,
				  )


class BookListView(LoginRequiredMixin, generic.ListView):
	model = Book
	paginate_by = 10
	context_object_name = 'book_list'  # your own name for the list as a template variable
	queryset = Book.objects.all()  # Get 5 books containing the title war


class BookDetailView(generic.DetailView):
	model = Book


class LoanedBooksByUserListView(LoginRequiredMixin, generic.ListView):
	"""
	Generic class-based view listing books on loan to current user.
	"""
	model = BookInstance
	template_name = 'catalog/bookinstance_list_borrowed_user.html'
	paginate_by = 10

	def get_queryset(self):
		return BookInstance.objects.filter(borrower=self.request.user).order_by('due_back')
